var DRYOBJ = ( function ( ) {
	
	/*################  ES5 pragma  ######################*/
	'use strict';	

	var _arVendorPREs = [ "moz", "ms", "webkit", "o" ];
	var _window = window; 
	var _navigator = window.navigator;

	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	function _RetPropertyRoutine( pm1, pm2 ) { 
		
		
			
		var ar_vendorPreez = _arVendorPREs; // [ "moz", "ms", "webkit", "o" ]  --> // object

		var clCharMax = ar_vendorPreez.length; 
		var leProp;
		var dL;
		var param = pm1; // getUserMedia
		var paramEl = pm2; // Navigator
		var len = param.length; 
		var nc = param.slice( 0,1 ); // g
		var Uc = param.slice( 0,1 ).toUpperCase(); // G
		var Param = param.replace( nc, Uc ); // GetUserMedia	
		if ( param in paramEl ) { 
			leProp = param; 
		} 
		for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
			if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
				leProp = ar_vendorPreez[ dL ] + Param; 
			} 
		} 
		return leProp;
	}

	function _ReturnVideoCodex( vidByID_pm ) { 
		
		var vdcdxTxt = "";
		if( vidByID_pm.canPlayType && vidByID_pm.canPlayType('video/webm; codecs="vp8, vorbis"') ) {
			return vdcdxTxt = "webm";
		}
		else 
		if( vidByID_pm.canPlayType && vidByID_pm.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"') ) {
			return vdcdxTxt = "mp4";
		}
		else 
		if( vidByID_pm.canPlayType && vidByID_pm.canPlayType('video/ogg; codecs="theora"') ) { 
			return vdcdxTxt = "ogv";
		}
		
		return vdcdxTxt;
		
	}

	// END of _private properties 
	return {
		
		Codices: { 
			ReturnVideoCodex : function( vidByID_pm ) {
				return _ReturnVideoCodex( vidByID_pm );
			}
		} , 

		ReturnProps : { 
			RetPropertyRoutine : function( pm1, pm2 ) {
				return _RetPropertyRoutine( pm1, pm2 );
			} 
		} , 

		RetELs : {
			glow : function() { 
				return _window; 
			} , 
			navEL : function() {
				return _navigator; 
			} 
		} ,

		Utils : {
			evt_u : {
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} 
			}
		}

	}; // END public properties
	
}( ) ); // console.log( DRYOBJ ); 

/* ###################################################### */
// FORMAT ONE

var Gh_pages_url = ( function() {	
	
	/*################  ES5 pragma  ######################*/
	'use strict';

	var _vid = null, _vidEXT = null;
	var _gUM_configObject = { 
		
		fallbackURLs : { 
			ogv : "https://artavia.gitlab.io/parallax/media/bnc_ogv/sanji-n-nami.ogv" 
			, mp4 : "https://artavia.gitlab.io/parallax/media/bnc_iOS_mp4/sanji-n-nami_iOS.mp4" 
			, webm : "https://artavia.gitlab.io/parallax/media/bnc_webM/sanji-n-nami.webm" 
		} 
		
		, vidELID : "camera-stream" 
		
		, ConstraintsObjPM : { video: true } , // ConstraintsObjPM : { audio: true, video: true } , 

		CallbacksObj : { 
			SuccesscallbackPM : function ( localMediaStream ) { 
				_vid = document.getElementById( _gUM_configObject.vidELID ); 

				// [ "URL" , "mozURL" , "msURL" , "webkitURL" , "oURL" ] 
				var URL = DRYOBJ.ReturnProps.RetPropertyRoutine( "URL" , DRYOBJ.RetELs.glow() ); 

				// Create an object URL for the video stream... AND Use this to set the video source. 
				if( window[ URL ] !== "undefined") { 

					// 2nd condition makes this choke in Opera 12.x!!! 
					// CHOKE!!! This applies to supporting compliant browsers
					if( window[ URL ] && window[ URL ].createObjectURL ) { 
						_vid.src = window[ URL ].createObjectURL(localMediaStream); 
					} 
					else { 
						_vid.src = localMediaStream;
					} 
				} 
			} , 
			
			ErrorcallbackPM : function ( derrpMsg ) { 
				alert( "Hey, man - this occurred when trying to use getUserMedia: " + derrpMsg );
			} 
		}

	};
	
	function _LOAD() {
		
		// [ "getUserMedia" , "mozGetUserMedia" , "msGetUserMedia" , "webkitGetUserMedia" , "oGetUserMedia" ] 
		var gUM = DRYOBJ.ReturnProps.RetPropertyRoutine( "getUserMedia" , DRYOBJ.RetELs.navEL() );

		if ( !!gUM ) { 
			DRYOBJ.RetELs.navEL()[ gUM ]( 
				_gUM_configObject.ConstraintsObjPM , 
				_gUM_configObject.CallbacksObj.SuccesscallbackPM , 
				_gUM_configObject.CallbacksObj.ErrorcallbackPM 
			); 
		}
		else { 
			_vid = document.getElementById( _gUM_configObject.vidELID ), _vidEXT = DRYOBJ.Codices.ReturnVideoCodex( _vid ); 
			if ( !!_vidEXT ) { 
				_vid.controls = true;
				if ( !!(_vidEXT === "webm") ) { 
					_vid.src = _gUM_configObject.fallbackURLs.webm;
					return;
				}
				if ( !!(_vidEXT === "mp4") ) { 
					var sniffer = DRYOBJ.Utils.SnifferREGEX(); 
					_vid.src = _gUM_configObject.fallbackURLs.mp4;
					return; 
				}
				if ( !!(_vidEXT === "ogv") ) { 
					_vid.src = _gUM_configObject.fallbackURLs.ogv;
					return;
				} 
			} 
		}

	}
	// END of _private properties

	return {
		InitLoad: function() {
			return _LOAD(); 
		} // window.Gh_pages_url.InitLoad()
	};
} )(); // window.Gh_pages_url


DRYOBJ.Utils.evt_u.AddEventoHandler( window, "load" , Gh_pages_url.InitLoad() ); 