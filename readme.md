# Brief description
The URL object contained in window is an ubiquitous interface that all developers can access with a little preparation.


## Please visit
You can see [the project](https://artavia.gitlab.io/gum_basic "the project at github") for yourself and decide whether it is really for you or not. 


## The interface is not standards&#45;compliant in all browsers
Hence, a little preparation on our part is necessary in order to help this method come out into the open and play for a while. Enter: proprietary-prefixes. 

### The URL object
The URL object is everywhere these days. And, it is capable of so much. It permits a lot of operations that we are already used to performing. The difference, however, is in the quantity of data that these re&#45;expressed operations will put on the wire. Apparently, XHR requests as we know them can be refactored as a byte array which will facilitate the same outcome but with a lighter footprint or size. This is exciting. 

#### Where is it used?
Apparently, the URL object is used in the context of Web-RTC, the Media Stream component being the first of three to make it work. Also, it is used in the context of the File Reader, File Sync, and File Writer APIs. If I am not mistaken, in lieu of working with text in the drag and drop api, and opting for raw data (I am paraphrasing) instead, I hear this can permit some neat things, too. Finally, in lieu of xhr or ajax as we know it, the normal Web Worker API can facilitate some performance friendly handling and offloads in general. 

## Final caveat
I realize that in 2018 the **URL.createObjectURL** method is deprecated. Since I have bigger fish to fry at the moment, I will respectfully refer you to the lesson called [file api today](gitlab.com/artavia/file-api-today "link to file api today") where I utilized the new API. Once I am able to do so, I will come back and **fix this repo**, ideally before the deprecated API goes bust around July 2018 as [google has mentioned](https://www.chromestatus.com/features/5618491470118912 "link to public notice regarding Deprecate and Remove URL.createObjectURL for MediaStream").